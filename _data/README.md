# mutualaidlynn.org/\_data

In Eleventy, the [data directory](https://www.11ty.dev/docs/config/#directory-for-global-data-files) is meant for [global data](https://www.11ty.dev/docs/data-global/), making them available to all templates via Eleventy’s `data` object.

_See the Eleventy documentation for more information about the [data cascade](https://www.11ty.dev/docs/data-cascade/)._
