# mutualaidlynn.org/\_includes/collections

In Eleventy, [collections](https://www.11ty.dev/docs/collections/) use a `tags`  property in [front matter data](https://www.11ty.dev/docs/data-frontmatter/) and [template/directory data](https://www.11ty.dev/docs/data-template-dir/) to group pieces of content.

Combined with [`dataDeepMerge`](https://www.11ty.dev/docs/data-deep-merge/) in [`.eleventy.js`](https://gitlab.com/reubenlillie/mutualaidlynn.org/-/blob/master/.eleventy.js), these `tags` help establish the site navigation and breadcrumb trails.

Eleventy also allows for configuring advanced collections. That’s what this directory does. For example, [`i18n.js`](https://gitlab.com/reubenlillie/mutualaidlynn.org/-/blob/master/_includes/i18n.js) registers collections for the different translations to help filter and sort content accordingly.

The [`index.js`](https://gitlab.com/reubenlillie/mutualaidlynn.org/-/blob/master/_includes/collections/index.js) file here registers the modules in this directory with [`_includes/index.js`](https://gitlab.com/reubenlillie/mutualaidlynn.org/-/blob/master/_includes/index.js), which, in turn, registers them with Eleventy in `.eleventy.js`.
