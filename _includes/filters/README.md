# mutualaidlynn.org/\_includes/filters

In Eleventy, [filters](https://www.11ty.dev/docs/filters/) modify content before templates are processed. Filters are accessible in templates via the JavaScript keyword `this`.

The [`index.js`](https://gitlab.com/reubenlillie/mutualaidlynn.org/-/blob/master/_includes/filters/index.js) file here registers the filters in this directory with [`_includes/index.js`](https://gitlab.com/reubenlillie/mutualaidlynn.org/-/blob/master/_includes/index.js), which, in turn, registers them with Eleventy in [`.eleventy.js`](https://gitlab.com/reubenlillie/mutualaidlynn.org/-/blob/master/.eleventy.js).
