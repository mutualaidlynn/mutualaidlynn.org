# mutualaidlynn.org/\_includes/transforms

In Eleventy, [transforms](https://www.11ty.dev/docs/config/#transforms) modify content after templates are processed. While [filters](https://www.11ty.dev/docs/filters) modify content _before_ templates are processed and are accessible via the JavaScript keyword `this`, transforms run automatically.

The [`index.js`](https://gitlab.com/reubenlillie/mutualaidlynn.org/-/blob/master/_includes/transforms/index.js) file here registers the transforms in this directory with [`_includes/index.js`](https://gitlab.com/reubenlillie/mutualaidlynn.org/-/blob/master/_includes/index.js), which, in turn, registers them with Eleventy in [`.eleventy.js`](https://gitlab.com/reubenlillie/mutualaidlynn.org/-/blob/master/.eleventy.js).
