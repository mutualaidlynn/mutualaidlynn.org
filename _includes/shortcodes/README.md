# mutualaidlynn.org/\_includes/shortcodes

In Eleventy, [shortcodes](https://www.11ty.dev/docs/shortcodes/) provide reusable bits of content. Shortcodes are accessible in templates via the JavaScript keyword `this`.

The [`index.js`](https://gitlab.com/reubenlillie/mutualaidlynn.org/-/blob/master/_includes/shortcodes/index.js) file here registers the shortcodes in this directory with [`_includes/index.js`](https://gitlab.com/reubenlillie/mutualaidlynn.org/-/blob/master/_includes/index.js), which, in turn, registers them with Eleventy in [`.eleventy.js`](https://gitlab.com/reubenlillie/mutualaidlynn.org/-/blob/master/.eleventy.js).
