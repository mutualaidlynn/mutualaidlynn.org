# mutualaidlynn.org/\_includes/

In Eleventy, the [includes directory](https://www.11ty.dev/docs/config/#directory-for-includes) is meant for reusable layouts and other extentions. Templates can use these files, but the files themselves will not be processed by Eleventy as templates in the [output directory](https://www.11ty.dev/docs/config/#output-directory).

The [`index.js`](https://gitlab.com/reubenlillie/mutualaidlynn.org/-/blob/master/_includes/index.js) file here is used to configure modules in this directory with Eleventy (in [`.eleventy.js`](https://gitlab.com/reubenlillie/mutualaidlynn.org/-/blob/master/.eleventy.js)).

The directories correspond to the type of feature supported by Eleventy.

* [Collections](https://www.11ty.dev/docs/collections/)
* [Filters](https://www.11ty.dev/docs/filters/)
* [Layouts](https://www.11ty.dev/docs/layouts/)
* [Shortcodes](https://www.11ty.dev/docs/shortcodes/)
* [Transforms](https://www.11ty.dev/docs/config/#transforms)
