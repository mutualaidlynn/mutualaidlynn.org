# mutualaidlynn.org/\_includes/layouts

In Eleventy, [layouts](https://www.11ty.dev/docs/layouts/) are reusable templates that can also be used to wrap other content.

Layouts can be accessed inside templates and [chained with other layouts](https://www.11ty.dev/docs/layout-chaining/) via the `layout` property in Eleventy’s `data` object.

Eleventy supports a number of different [template languages](https://www.11ty.dev/docs/languages/). This project specifically leverages [JavaScript templates](https://www.11ty.dev/docs/languages/javascript/) (files ending in the `.11ty.js`) for speed, flexibility, and resilience. 
