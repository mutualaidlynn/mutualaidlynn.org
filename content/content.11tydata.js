/**
 * @file Contains metadata common to all site content
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * An Eleventy directory data module
 * @module content/
 * @see {@link https://www.11ty.dev/docs/data-template-dir/ Template and directory data files in 11ty}
 */
module.exports = {
  locale: 'en',
  permalink: '/{{locale}}/{% if slug %}{{slug|slug}}{% else %}{{page.fileSlug}}{% endif %}/index.html'
}
