# mutualaidlynn.org/content

The `content/` directory contains the raw text for the site.

Each translation is stored in a directory named by a corresponding [ISO 639-2 Language Code](https://www.loc.gov/standards/iso639-2/php/code_list.php).

Likewise, the [`_data`](https://gitlab.com/reubenlillie/mutualaidlynn.org/-/tree/master/_data) directory should contain a matching `.js` file for each translation.

## File Types

Files ending in `.md` are written in [Markdown](https://daringfireball.net/projects/markdown/), a form of plain text which Eleventy uses to generate HTML. Markdown files may also contain [front matter data](https://www.11ty.dev/docs/data-frontmatter/) (located between `---` at the top of a file).

Files ending in `.json` or `.11tydata.js` leverage [Eleventy’s data cascade](https://www.11ty.dev/docs/data-cascade/) for [template and directory data](https://www.11ty.dev/docs/data-template-dir/).

## A Note for Translators

Only edit the raw text in a translation directory.

Do _not_ change file names or locations; file paths should match between translations.

Do _not_ change data key names (text before the colon `:` character in `.json`, or `.11tydata.js` files as well as in the front matter section in `.md` files).

When editing data values (text after a `:`), keep any wrapping quotes in tact (double quotes in `.json` files or single quotes in `.11tydata.js` files).

However, do _not_ change the value for the `translationKey` property. This value must remain identical in order to link content across translations.
