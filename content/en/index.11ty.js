/**
 * @file Defines the chained template for the English home page
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in 11ty}
 */

/**
 * Acts as front matter in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in 11ty}
 */
exports.data ={
  permalink: '/',
  templateEngineOverride: '11ty.js,md',
  title: 'Welcome to Lynn Mutual Aid',
}

/**
 * The content of the English home page template
 * @method
 * @name render()
 * @param {Object} data 11ty’s data object
 * @return {String} The rendered template
 * @see {@link https://www.11ty.dev/docs/quicktips/not-found/ 404 pages in 11ty}
 */
exports.render = data => `<!-- content/en/index.11ty.js -->
Welcome to Lynn, MA Mutual Aid and Disaster Relief Group ! In these fast moving and uncertain times, it’s important that we show up for each other and remember that we are not alone. 

Mutual aid is a powerful way to build strong connections- we all have something to offer and we all have something we need. This group is still building out infrastructure, so please look out for updates!
`
