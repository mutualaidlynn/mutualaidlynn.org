/**
 * @file Contains metadata common to all English-language content
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * An Eleventy directory data module
 * @module content/en
 * @see {@link https://www.11ty.dev/docs/data-template-dir/ Template and directory data files in 11ty}
 */
module.exports = {
  locale: 'en',
  permalink: '/{{page.fileSlug}}/index.html'
}
