/**
 * @file Defines the chained template for the Spanish home page
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in 11ty}
 */

/**
 * Acts as front matter in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in 11ty}
 */
exports.data ={
  permalink: '/es/',
  templateEngineOverride: '11ty.js,md',
  title: '¡Bienvenido a Lynn, MA Grupo de Ayuda Mutual!',
}

/**
 * The content of the Spanish home page template
 * @method
 * @name render()
 * @param {Object} data 11ty’s data object
 * @return {String} The rendered template
 * @see {@link https://www.11ty.dev/docs/quicktips/not-found/ 404 pages in 11ty}
 */
exports.render = data => `<!-- content/es/index.11ty.js -->
¡Bienvenido a Lynn, MA Grupo de Ayuda Mutua y Alivio de Desastres! En estos tiempos inciertos y donde información cambia rápidamente, es importante estar unidos y recordar que no estamos solos.

La ayuda mutua es una forma poderosa de construir conexiones sólidas: todos tenemos algo que ofrecer y todos tenemos algo que necesitamos. Este grupo todavía está construyendo infraestructura necesarios para llevar a cob nuestra ideas , ¡así que esten pendientes de las actualizaciones!
`
