/**
 * @file Configures Eleventy options and helper methods
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 */

/**
 * 11ty’s configuration module
 * @module .eleventy
 * @param {Object} eleventyConfig 11ty’s Config API
 * @return {Object} {} 11ty’s optional Config object
 * @see {@link https://www.11ty.dev/docs/config/}
 */
module.exports = function (eleventyConfig) {

  /**
   * Deep merge data in the cascade, particularly important for tags
   * @see {@link https://www.11ty.dev/docs/data-deep-merge/ Data deep merge in 11ty docs}
   */
  eleventyConfig.setDataDeepMerge(true)

  return

}
