Welcome to Lynn, MA Mutual Aid and Disaster Relief Group! In these fast moving and uncertain times, it’s important that we show up for each other and remember that we are not alone. 

¡Bienvenido a Lynn, MA Grupo de Ayuda Mutua y Alivio de Desastres! En estos tiempos inciertos y donde información cambia rápidamente, es importante estar unidos y recordar que no estamos solos.

[[_TOC_]]

## About

Mutual aid is a powerful way to build strong connections—we all have something to offer and we all have something we need. This group is still building out infrastructure, so please look out for updates!

Many thanks to MAMAS (mutual aid Medford and Somerville) for sharing the resources to create this platform!

La ayuda mutua es una forma poderosa de construir conexiones sólidas: todos tenemos algo que ofrecer y todos tenemos algo que necesitamos. Este grupo todavía está construyendo infraestructura necesarios para llevar a cob nuestra ideas , ¡así que esten pendientes de las actualizaciones! 

¡Muchas gracias a MAMAS (ayuda mutua Medford y Somerville) por compartir los recursos para crear esta plataforma! 

## Website Project Summary

This site is designed to be fast, secure, accessible, and to have small cyber and carbon footprints.

All content for this site is prerendered using the static site generator [Eleventy](https://11ty.dev/).

The templates are written entirely in vanilla JavaScript (files with the [`*.11ty.js`](https://www.11ty.dev/docs/languages/javascript/) extension).

## Local development

Install [Node.js](https://nodejs.org/) on your machine (see [11ty documentation for specific requirements](https://www.11ty.dev/docs/getting-started/)).

Then enter the following commands into your terminal:

### Getting started

#### 1. Clone this repository and all its dependencies.

```cli
git clone git@gitlab.com:reubenlillie/mutualaidlynn.org.git
```

#### 2. Go to the working directory.

```cli
cd mutualaidlynn.org
```

#### 3. Install the project dependencies with [NPM](https://www.npmjs.com/).

```
npm install
```

#### 4. Start your local server.

```cli
npx eleventy --serve
```

&copy; 2020 by Mutual Aid Lynn
