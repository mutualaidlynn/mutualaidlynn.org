/**
 * @file Defines the chained template for the 404 page
 * @author Reuben L. Lillie <reubenlillie@gmail.com>
 * @see {@link https://www.11ty.dev/docs/layouts/#layout-chaining Layout chaining in 11ty}
 */

/**
 * Acts as front matter in JavaScript templates
 * @see {@link https://www.11ty.dev/docs/languages/javascript/#optional-data-method Optional `data` in JavaScript templates in 11ty}
 */
exports.data = {
  eleventyExcludeFromCollections: true,
  locale: 'en',
  permalink: '404.html',
  templateEngineOverride: '11ty.js,md',
  title: 'Oops! Page not found! ¡La página no encontrada!'
}

/**
 * The content of the 404 page template
 * @method
 * @name render()
 * @param {Object} data 11ty’s data object
 * @return {String} The rendered template
 * @see {@link https://www.11ty.dev/docs/quicktips/not-found/ 404 pages in 11ty}
 */
exports.render = data => `<!-- content/404.11ty.js -->
  Sorry, the content you are looking for cannot be found at that address.
  
  Lo sentemos, el continiero que está buscando no se puede entcontrar en el sitio web.
  
  Do you want to try one of these options instead?
  
  ¿Quieres probar una de estes opciones?

  * [Go to the home page](/)
  * [Ir al la página principal](/es/)
`
